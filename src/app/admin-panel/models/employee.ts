export interface Employee {
  id: number;
  name: string;
  surname: string;
  email: string;
  status: string; // 'Kierownik' / 'Pracownik'
}
