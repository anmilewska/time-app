import { Component } from '@angular/core';
import { SidebarLink } from '../../models/sidebar-link';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent {
  sidebarLinks: SidebarLink[] = [
    {
      group: 'Raporty',
      links: [
        { name: 'Aktywne', href: '' },
        { name: 'Zamknięte', href: '' },
      ],
    },
    {
      group: 'Panel kierownika',
      links: [{ name: 'Raporty', href: 'leader-panel' }],
    },
    {
      group: 'Panel admina',
      links: [{ name: 'Użytkownicy', href: 'admin-panel' }],
    },
  ];
}
